#!/usr/bin/env python
# -*- coding: utf-8 -*-
import subprocess
import datetime
import time

processos = "ps -e -o pid,time,uname,cmd,pmem,pcpu --sort=-pmem,-pcpu"

processos_ociosos = subprocess.Popen(processos, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               close_fds=False)
array_processos = []
processos_line = []
processos_lista = []
processos_a_serem_encerrados = []

# decodificar bytes processos e adicionar a array
for p in processos_ociosos.communicate():
	array_processos.append(p.decode("utf-8"))
	break

# quebrar em linhas array de processos
for arr in array_processos:
	processos_line.append(arr.splitlines())

# printa linhas de processos do array por processo
for line_proc in processos_line:
	for i in range(len(line_proc)):
		PID = line_proc[i].split()[0]
		TIME = line_proc[i].split()[1]
		USER = line_proc[i].split()[2]
		CMD = line_proc[i].split()[3]
		MEM = line_proc[i].split()[4]
		CPU = line_proc[i].split()[5]
		processos_lista.append({'PID': PID, 'TIME': TIME, 'USER': USER, 'CMD': CMD, 'MEM': MEM, 'CPU': CPU})


# remove a primeira index da lista do array de processos_lista
processos_lista.pop(0)

#cria array dos processos zumbis com tempo de execução acima de 10 minutos
tempo_maximo_para_processos = datetime.datetime.strptime('00:10:00', '%H:%M:%S').time()
for proc in processos_lista:
	time_processo = datetime.datetime.strptime(proc['TIME'], '%H:%M:%S').time()
	if time_processo > tempo_maximo_para_processos:
		processos_a_serem_encerrados.append(proc['PID'])

if len(processos_a_serem_encerrados) > 0:
	pids_a_encerrar = str(processos_a_serem_encerrados).replace("u", "").replace("[", "").replace("]", "").replace("'", "").replace(",", " ")

	processos_a_encerrar = "kill " + pids_a_encerrar

	encerrar_zumbi_pid = subprocess.Popen(processos_a_encerrar, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
								   stderr=subprocess.STDOUT,
								   close_fds=False)
	print (encerrar_zumbi_pid)