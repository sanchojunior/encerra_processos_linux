#instalar postgresql-contrib 
#sudo apt-get install postgresql postgresql-contrib
# -*- coding: utf-8 -*-
import subprocess
import datetime
server = '192.168.0.13'
encerra_queries_ociosas = "SELECT now() - query_start as tempo_executado_query, procpid FROM pg_stat_activity"

array_processos = []
processos_line = []
processos_lista = []
processos_a_serem_encerrados = []
pids_terminate = []

conect_db_portal = 'psql -U postgres -h ' + server + ' -c ' + "'" + encerra_queries_ociosas + "'"

processos_ociosos = subprocess.Popen(conect_db_portal, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               close_fds=False)

# decodificar bytes processos e adicionar a array
for p in processos_ociosos.communicate():
	array_processos.append(p.decode("utf-8"))
	break

# quebrar em linhas array de processos
for arr in array_processos:
	processos_line.append(arr.splitlines())

# printa linhas de processos do array por processo
for line_proc in processos_line:
	for i in range(len(line_proc)):
		processos_lista.append(line_proc[i].split())

processos_lista.pop(0)
processos_lista.pop(0)
processos_lista.reverse()
processos_lista.pop(0)
processos_lista.pop(0)

for proc in processos_lista:
	TEMPO_EXECUTADO_QUERY = proc[0]
	PROC_PID = proc[2]
	processos_a_serem_encerrados.append({'TEMPO_EXECUTADO_QUERY': TEMPO_EXECUTADO_QUERY, 'PROC_PID': PROC_PID})

#cria array dos processos zumbis com tempo de execução acima de 30 minutos
tempo_maximo_para_processos = datetime.datetime.strptime('00:30:00', '%H:%M:%S').time()
for proc in processos_a_serem_encerrados:
	string_tempo_query = len(proc['TEMPO_EXECUTADO_QUERY'])
	proc['TEMPO_EXECUTADO_QUERY'] = proc['TEMPO_EXECUTADO_QUERY'].replace(
										proc['TEMPO_EXECUTADO_QUERY'][8:string_tempo_query], '')
	time_processo = datetime.datetime.strptime(proc['TEMPO_EXECUTADO_QUERY'], '%H:%M:%S').time()

	if time_processo > tempo_maximo_para_processos:
		pids_terminate.append(proc['PROC_PID'])

if len(pids_terminate) > 0:
	pids_terminate = str(pids_terminate).replace("[", "").replace("]", "").replace("'","")

	encerra_queries_ociosas = "select pg_terminate_backend("+pids_terminate+")"

	encerra_pids_db_portal = 'psql -U postgres -h ' + server + ' -c ' + "'" + encerra_queries_ociosas + "'"

	encerrar_zumbi_pid = subprocess.Popen(encerra_pids_db_portal, shell=True, stdin=subprocess.PIPE,
										  stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=False)